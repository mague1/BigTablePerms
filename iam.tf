# Writer
resource "google_service_account" "bigtable_writer" {
  project      = var.project_name
  account_id   = "bt-writer"
  display_name = "BigTable Writer"
}

resource "google_project_iam_binding" "bigtable_writer" {
  project = var.project_name
  role    = "roles/bigtable.user"

  members = [
    "serviceAccount:${google_service_account.bigtable_writer.email}"
  ]

}

resource "google_service_account_key" "bigtable_writer" {
  service_account_id = google_service_account.bigtable_writer.name
}

# Reader
resource "google_service_account" "bigtable_reader" {
  project      = var.project_name
  account_id   = "bt-reader"
  display_name = "BigTable Reader"
}

resource "google_project_iam_binding" "bigtable_reader_restricted" {
  project = var.project_name
  role    = "roles/bigtable.reader"

  members = [
    "serviceAccount:${google_service_account.bigtable_reader.email}"
  ]

  condition {
    title       = "Read Access for restricted table"
    description = "Allow the reader user to only read the restricted table"
    expression  = "resource.type == 'bigtableadmin.googleapis.com/Table' && resource.name.startsWith('projects/${var.project_name}/instances/${google_bigtable_instance.instance.name}/tables/restricted') && resource.service == 'bigtableadmin.googleapis.com'"

  }
}

resource "google_project_iam_binding" "bigtable_reader_everyone" {
  project = var.project_name
  role    = "roles/bigtable.user"

  members = [
    "serviceAccount:${google_service_account.bigtable_reader.email}"
  ]

  condition {
    title       = "write Access for everyone table"
    description = "Allow the reader user to only read the restricted table"
    expression  = "resource.type == 'bigtableadmin.googleapis.com/Table' && resource.name.startsWith('projects/${var.project_name}/instances/${google_bigtable_instance.instance.name}/tables/everyone') && resource.service == 'bigtableadmin.googleapis.com'"

  }
}

resource "google_service_account_key" "bigtable_reader" {
  service_account_id = google_service_account.bigtable_reader.name
}

# Template
resource "local_sensitive_file" "bigtable_writer" {
  content  = base64decode(google_service_account_key.bigtable_writer.private_key)
  filename = "${path.module}/writer.json"
}

resource "local_sensitive_file" "bigtable_reader" {
  content  = base64decode(google_service_account_key.bigtable_reader.private_key)
  filename = "${path.module}/reader.json"
}


