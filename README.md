# Big Table Permissions test

This is an example of controlling user level access in BigTable with Terraform based on [Google Blog](https://cloud.google.com/blog/products/databases/iam-techniques-for-cloud-bigtable)



## Setting Up

```
cp tfvars.example my.tfvars
```

Edit this file to set your project and the location of your credentials file

```
terraform init
terraform apply -var-file=my.tfvars
```

## Infrastructure

Running this terraform will create a BigTable instance named after your project and two table `restricted` and `everyone`.

In addition two service accounts `reader` and `writer` will be created and their private keys will be written on the local disk.

User `reader` will have read access only to the `restricted` table and read/write access to the `everyone` table

User `writer` will have read/write access to the `restricted` table and read/write access to the `everyone` table

![](perms.png)

## Testing

Terraform will output commands similar to the following

```
Outputs:

tables-list = "cbt -creds writer.json -project myproject -instance  myproject-bt ls"
tables-read-1 = "cbt -creds reader.json -project myproject -instance  myproject-bt read restricted"
tables-read-2 = "cbt -creds reader.json -project myproject -instance  myproject-bt read everyone"
tables-write-1 = "cbt -creds writer.json -project myproject -instance  myproject-bt set restricted row1 rf1:writer=writer"
tables-write-2 = "cbt -creds writer.json -project myproject -instance  myproject-bt set everyone row1 ef1:writer=writer"
tables-write-3 = "cbt -creds reader.json -project myproject -instance  myproject-bt set everyone row1 ef1:reader=reader"
```

Try running the commands and observe the results

Try writing to the `restricted` table with the `reader` user to see if it's possible


## Using in Production

If you are using GKE it is recommend to use [Workload Identity Federation](https://cloud.google.com/kubernetes-engine/docs/concepts/workload-identity) or if you are using VMs you can attach the service account to the virtual machine in [terraform](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance#service_account).

It is not recommened to write the key file out to disk.
