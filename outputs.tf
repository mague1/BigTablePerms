output "tables-list" {
  value = "cbt -creds writer.json -project ${var.project_name} -instance  ${var.project_name}-bt ls"
}

output "tables-write-1" {
  value = "cbt -creds writer.json -project ${var.project_name} -instance  ${var.project_name}-bt set restricted row1 rf1:writer=writer"
}

output "tables-write-2" {
  value = "cbt -creds writer.json -project ${var.project_name} -instance  ${var.project_name}-bt set everyone row1 ef1:writer=writer"
}

output "tables-write-3" {
  value = "cbt -creds reader.json -project ${var.project_name} -instance  ${var.project_name}-bt set everyone row1 ef1:reader=reader"
}

output "tables-read-1" {
  value = "cbt -creds reader.json -project ${var.project_name} -instance  ${var.project_name}-bt read restricted"
}

output "tables-read-2" {
  value = "cbt -creds reader.json -project ${var.project_name} -instance  ${var.project_name}-bt read everyone"
}
