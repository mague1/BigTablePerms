variable "project_name" {
  type = string
}

variable "zone" {
  type    = string
  default = "us-west1-c"
}

variable "gcp_credentials_path" {
  type = string
}

variable "node_count" {
  type    = number
  default = 1
}

variable "node_storage_type" {
  type    = string
  default = "HDD"
}

variable "deletion_protection" {
  type    = bool
  default = false
}
