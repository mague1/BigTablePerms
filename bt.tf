resource "google_bigtable_instance" "instance" {
  project             = var.project_name
  name                = "${var.project_name}-bt"
  deletion_protection = var.deletion_protection

  cluster {
    cluster_id   = "${var.project_name}-bt-c"
    num_nodes    = var.node_count
    storage_type = var.node_storage_type
    zone         = var.zone
  }
  labels = {
    project = var.project_name
  }
}

resource "google_bigtable_table" "restricted" {
  name          = "restricted"
  instance_name = google_bigtable_instance.instance.name
  lifecycle {
    prevent_destroy = false
  }

  column_family {
    family = "rf1"
  }
}

resource "google_bigtable_table" "everyone" {
  name          = "everyone"
  instance_name = google_bigtable_instance.instance.name
  lifecycle {
    prevent_destroy = false
  }

  column_family {
    family = "ef1"
  }
}
