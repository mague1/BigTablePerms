provider "google" {
  credentials = file(var.gcp_credentials_path)
  project     = var.project_name
  region      = join("-", slice(split("-", var.zone), 0, 2))
}

locals {}
